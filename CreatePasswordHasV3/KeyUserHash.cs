﻿using System;
using System.Security.Cryptography;

namespace CreatePasswordHasV3
{
    public static class KeyUserHash
    {
        public static GralResponce HashPasswordV3(string password)
        {
            byte[] salt;
            byte[] buffer2;
            if (password == null)
            {
                return new GralResponce
                {
                    bResponce = false,
                    sMessage = "No se puede encriptar un password vacio"
                };
            }
            using (Rfc2898DeriveBytes bytes = new Rfc2898DeriveBytes(password, 0x10, 0x3e8))
            {
                salt = bytes.Salt;
                buffer2 = bytes.GetBytes(0x20);
            }
            byte[] dst = new byte[0x31];
            Buffer.BlockCopy(salt, 0, dst, 1, 0x10);
            Buffer.BlockCopy(buffer2, 0, dst, 0x11, 0x20);
            return new GralResponce
            {
                bResponce = true,
                sMessage = Convert.ToBase64String(dst)
            };
        }

        public static GralResponce VerifyHashedPassword(string hashedPassword, string password)
        {
            byte[] buffer4;

            if (hashedPassword == null || password == null)
            {
                return new GralResponce
                {
                    bResponce = false,
                    sMessage = "El password encriptado y el passaword, son necesarios para realizar la validacion de seguridad"
                };
            }
            byte[] src = Convert.FromBase64String(hashedPassword);
            if ((src.Length != 0x31) || (src[0] != 0))
            {
                return new GralResponce
                {
                    bResponce = false,
                    sMessage = "El passaword es incorrecto"
                };
            }
            byte[] dst = new byte[0x10];
            Buffer.BlockCopy(src, 1, dst, 0, 0x10);
            byte[] buffer3 = new byte[0x20];
            Buffer.BlockCopy(src, 0x11, buffer3, 0, 0x20);
            using (Rfc2898DeriveBytes bytes = new Rfc2898DeriveBytes(password, dst, 0x3e8))
            {
                buffer4 = bytes.GetBytes(0x20);
            }
            return new GralResponce
            {
                bResponce = ByteArraysAreEqual(buffer3, buffer4),
                sMessage = "Contraseña correcta"
            };
        }

        private static bool ByteArraysAreEqual(byte[] array1, byte[] array2)
        {
            if (array1.Length != array2.Length)
            {
                return false;
            }
            for (int i = 0; i < array1.Length; i++)
            {
                if (array1[i] != array2[i])
                {
                    return false;
                }
            }
            return true;
        }
    }
}
