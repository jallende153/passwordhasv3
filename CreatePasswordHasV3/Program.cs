﻿using System;

namespace CreatePasswordHasV3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ingrese la contraseña a proteger");
            var password = Console.ReadLine();
            var oResponce = KeyUserHash.HashPasswordV3(password);
            Console.WriteLine(oResponce.sMessage);
            Console.ReadLine();
        }
    }
}
